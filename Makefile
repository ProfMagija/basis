HC = stack ghc --
CC = clang

Main: Main.hs rt.o
	$(HC) $^

%.o: %.c
	$(CC) -c $< -o $@
