{-# LANGUAGE ForeignFunctionInterface #-}
module Main where

import Prelude
import Foreign.C
import Data.Char
import System.IO.Unsafe

newtype Token = Token Int deriving (Show)
newtype Event a = Event (a, Token) deriving (Show)

foreign import ccall "rt_putchar" rt_putchar :: CInt -> CInt -> CInt
foreign import ccall "rt_consume" rt_consume :: CInt -> IO ()

putchar :: Char -> Token -> Event ()
putchar c (Token tok) = Event ((), Token . fromIntegral $ rt_putchar (fromIntegral tok) $ fromIntegral . ord $ c) 

consume :: Int -> IO ()
consume = rt_consume . fromIntegral

class SyState s where
    putOutput :: Char -> s -> s

data SyStateImpl = SyStateImpl
    { outputChars   :: [Event Char] 
    , token         :: Token
    }

instance SyState SyStateImpl where
    putOutput ch state = state { outputChars = oc', token = tok' }
        where oc' = Event (ch, tok') : outputChars state
              tok' = Token . unevent $ putchar ch $ token state

printString :: SyState s => [Char] -> s -> s
printString (h:t) = printString t . putOutput h
printString []    = \x -> x

unevent (Event (_, Token t)) = t

myMain :: SyState s => s -> s
myMain = printString "Hello!\n"

main = do
    let st = SyStateImpl { outputChars = [], token = Token 0 }
        x = myMain st
     in mapM_ (consume . unevent) $ outputChars x

